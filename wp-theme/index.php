<?php get_header(); ?>
  
 <!-- banner start -->
	<div class="banner">
		<h1 class="banner__title">Международная online-конференция<br>о призвании и выборе профессии</h1>
		<p class="banner__subtext">«Ведь жизнь слишком коротка, чтобы делать то, что тебе не нравится!»</p>
		<div class="banner__video"></div>
		<span class="banner__date">17-20 июня, 2015</span>
	</div> <!-- banner end -->
	
	
	<!-- ABOUT START -->
	<section id="about" class="about container">
		<h2 class="about__title">О конференции</h2>
		<p class="about__text">
			Наша online-конференция для тех, кто находится в поиске себя и своего призвания. Четыре насыщенных дня обучения от мастеров своего дела, не выходя из дома. 
		</p>
		<p class="about__text">
Мы собрали успешных украинских и зарубежных предпринимателей и руководителей, коучей и психологов, экспертов рынка труда и HR-специалистов. Они поделятся своими знаниями и опытом, чтобы помочь вам найти собственный путь. 
		</p>
		<p class="about__text">
Найти любимое дело, и не уметь заработать на нём, или работать на высокооплачиваемой работе, но ненавидеть её? Частый вопрос и сомнения, что ставить на чаши весов. Мы поможем найти пересечение между вашими собственными желаниями и способностями, и потребностью рынка.
		</p>
	</section> <!-- end about section -->
	
	<div class="who clearfix">
		<div class="who__image"></div>
		<div class="who__text">
			<h2>ДЛЯ КОГО ЭТА КОНФЕРЕНЦИЯ?</h2>
			<p>Поиск своего призвания не ограничен ни возрастом, ни сферой, ни образованием. 
Когда ощущение того, что ты не на своем месте, заставляет отправляться на поиски себя, в дороге важно иметь опытных проводников. 
			</p>
			<p>
Мы хотим помочь всем тем, кто ищет и совершенствуется. Людям, которые хотят получать удовольствие от работы и видеть смысл в своей деятельности. Тем, кто хочет каждое утро просыпаться с желанием творить и созидать, реализовывать собственный потенциал, получая за это достойное вознаграждение. 
			</p>
		</div>
	</div>

<!--REWARDS START -->
	
	<section id="rewards" class="reward container clearfix">
		<h2>что вы получите?</h2>
		<div class="reward__uniqe-proposal reward__uniqe-proposal__one">
			<h3>Уникальные темы и лучшие спикеры</h3>
			<p>
				Все темы и доклады являются уникальными и подготовлены специально для данного мероприятия. 
			</p>
		</div>
		<div class="reward__uniqe-proposal reward__uniqe-proposal__two">
			<h3>доступ из любой точки мира</h3>
			<p>
				Для участия вам нужен лишь доступ в интернет.  У вас будет возможность прослушать все темы, познакомиться со спикерами, задать интересующие вас вопросы, не выходя из дома или офиса.
			</p>
		</div>
		<div class="reward__uniqe-proposal reward__uniqe-proposal__three">
			<h3>Комплексный подход к теме</h3>
			<p>
				Вы сможете проанализировать собственные способности и желания, а также получить информацию о рынке труда, перспективных нишах и возможностях для применения своих талантов.
			</p>
		</div>
		<div class="reward__uniqe-proposal reward__uniqe-proposal__four">
			<h3>Полный доступ к материалам</h3>
			<p>
				После конференции у вас будет доступ к видеозаписям всех вебинаров конференции.
			</p>
		</div>
		
	</section> <!-- rewards end -->
	
	<!-- ORGANIZERS START -->
	
	<div class="organizer">
		<h2 class="organizer__title">организатор</h2>
		<figure>
			<a href="https://www.facebook.com/START2G0" target="_blank"> 
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/start2go-logo.jpg" alt="организатор start2go">
			</a>
		</figure>
	</div> <!-- organizers end -->
	
	<!--PROGRAM START -->
	
	<h2 id="program">программа</h2>
	
	<div role="program">
		
		<!-- Nav tabs -->
		<div class="container">
			<ul class="program__nav clearfix">


					<li role="presentation" class="nav__tab nav__tab__list nav__tab__active">
						<div class="nav__tab__title">
							<a href="#home" aria-controls="memory">
								блок 1. понять
							</a>
						</div>
						<p>как разобраться в себе и собственных желаниях.</p>
						<div class="nav__tab__date">
							<a href="#home" aria-controls="memory">
							17 июня						
							</a>
						</div>
					</li>

					<li role="presentation" class="nav__tab nav__tab__lance">
						<div class="nav__tab__title">
							<a href="#profile" aria-controls="profile">
								блок 2. узнать
							</a>
						</div>
						<p>о современном рынке труда, новых профессиях и требованиях к соискателям. </p>
						<div class="nav__tab__date">
							<a href="#profile" aria-controls="profile">
							18 июня						
							</a>
						</div>
					</li>

					<li role="presentation" class="nav__tab nav__tab__to-do">
						<div class="nav__tab__title">
							<a href="#messages" aria-controls="messages">
								блок 3. сделать
							</a>
						</div>
						<p>желаемое действительным с помощью инструментов личной эффективности.</p>
						<div class="nav__tab__date">
							<a href="#messages" aria-controls="messages">
								19 июня						
							</a>
						</div>
					</li>
					<li role="presentation" class="nav__tab nav__tab__paper-plane">
						<div class="nav__tab__title">
							<a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
								блок 4. вдохновиться
							</a>
						</div>
						<p>людьми, которые любят свое дело и каждый день совершенствуются в нем.</p>
						<div class="nav__tab__date">
							<a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
								20 июня
							</a>						
						</div>
					</li>
			</ul>
		</div>
			
			
			
			<!-- Tab panes -->
		<div class="program__content">
			<div class="container">
				<h2 class="program__content__title">Темы</h2>
				<div class="program__content__tab program__content__tab__active" id="memory">
				
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/valensa.png" alt="валенса"></figure>
						<p class="content__item__name">Анна Валенса, создатель центра развития  «Эпоха»
</p>
						<p class="content__item__title">
							«Самопознание. Искусство изменений»</p>
						<p class="content__item__description">Как найти в себе силы и мотивацию изменить свою жизнь к лучшему и сделать то, что давно хотелось.</p>
						<span class="content__item__time">19:00</span>

					</div>
					
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/chudnyavcev.png" alt=""></figure>
						<p class="content__item__name">Сергей Чуднявцев, психолог</p>
						<p class="content__item__title">
							«Чего я хочу? Выявление истинных желаний»</p>
						<p class="content__item__description">Как разобраться какие желания навязаны извне, а чего хочется именно вам. </p>
						<span class="content__item__time">20:00</span>

					</div>
					
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/brovchenko.png" alt=""></figure>
						<p class="content__item__name">Наталья Сирица,  HR-консультант</p>
						<p class="content__item__title">
							«Что я могу? Анализ собственных способностей»</p>
						<p class="content__item__description">Как оценить себя, разобраться в собственных сильных сторонах и сторонах, требующих развития. </p>
						<span class="content__item__time">21:00</span>

					</div>
					
				</div> <!-- first tab end -->
				
				<div class="program__content__tab" id="profile">
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/mironov.png" alt=""></figure>
						<p class="content__item__name">Михаил Миронов, CEO Brainify.ru</p>
						<p class="content__item__title">
							«Что нужно знать про будущее образования?» </p>
						<p class="content__item__description">Тренды, перспективы и изменения, к которым необходимо быть готовым.  </p>
						<span class="content__item__time">19:00</span>

					</div>
					
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/zakarcheva.png" alt=""></figure>
						<p class="content__item__name">Виктория Закарчевная, Newbreed </p>
						<p class="content__item__title">
							«Чего хотят работодатели от соискателей?» </p>
						<p class="content__item__description">Требования рынка и советы о том, как им соответствовать. Lifehacks от рекрутера.</p>
						<span class="content__item__time">20:00</span>

					</div>
					
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/incognito.png" alt=""></figure>
						<p class="content__item__name"></p>
						<p class="content__item__title">
							«Поколение Y. Возможности и угрозы»</p>
						<p class="content__item__description">Что нужно знать о себе, чтобы перестать себя ломать и найти наилучшее<br> применение своим качествам и способностям. </p>
						<span class="content__item__time">21:00</span>

					</div>
				</div> <!-- second tab end -->
				
				<div class="program__content__tab" id="messages">
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/burdeinaya.png" alt=""></figure>
						<p class="content__item__name">Алена Бурдейная, сооснователь клуба любителей бега KM Running Club</p>
						<p class="content__item__title">
							«Маленькая цель тебя погубит»</p>
						<p class="content__item__description">Почему важно ставить большие цели и как не попасть в ловушку собственных ограничений.</p>
						<span class="content__item__time">19:00</span>

					</div>
					
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/sudarkin.png" alt=""></figure>
						<p class="content__item__name">Александр Сударкин, бизнес-тренер</p>
						<p class="content__item__title">
							«Синергия личных и профессиональных целей»</p>
						<p class="content__item__description">Значение профессионального развития на пути к реализации собственного потенциала. Или как полюбить то, что делаешь.</p>
						<span class="content__item__time">20:00</span>

					</div>
					
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/agoeva.png" alt=""></figure>
						<p class="content__item__name">Яна Агоева, HR-консультант</p>
						<p class="content__item__title">
							«Эффективный карьерный план» </p>
						<p class="content__item__description">Инструменты и рекомендации по планированию и реализации карьерных целей. </p>
						<span class="content__item__time">21:00</span>

					</div>
				</div> <!-- third tab end -->
				
				<div class="program__content__tab" id="settings">
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/suromyatnikova.png" alt=""></figure>
						<p class="content__item__name">Яна Сыромятникова, совладелица свадебного агентства «ZEFIR»</p>
						<p class="content__item__title">
							«Как сделать праздник своей работой?»</p>
						<p class="content__item__description"></p>
						<span class="content__item__time"></span>

					</div>
					
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/petrova.png" alt=""></figure>
						<p class="content__item__name">Анна Петрова, CEO Startup Ukraine</p>
						<p class="content__item__title">
«Вложить душу, как лучшая инвестиция в бизнес»
</p>
						<p class="content__item__description"></p>
						<span class="content__item__time"></span>

					</div>
					
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/zara.png" alt=""></figure>
						<p class="content__item__name">Зарангиз Апетрей Хусейнова, CEO «ALZA» и «WoWoman» (Азербайджан)</p>
						<p class="content__item__title">
							«Как преодолеть стереотипы общества на пути к собственной реализации»</p>
						<p class="content__item__description"></p>
						<span class="content__item__time"></span>

					</div>
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/shlapay.png" alt=""></figure>
						<p class="content__item__name">Любовь Шлапай, маркетинг-директор Академии Mindvalley (Малайзия)</p>
						<p class="content__item__title">
							«Самообразование как инструмент достижения целей и новых возможностей»</p>
						<p class="content__item__description"></p>
						<span class="content__item__time"></span>

					</div>
					<div class="content__item clearfix">
						<figure class="content__item__img"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/lobova.png" alt=""></figure>
						<p class="content__item__name">Лена Лобова, COO и управляющий партнер «iLogos»</p>
						<p class="content__item__title">
							«Как развить компанию, развивая себя»</p>
						<p class="content__item__description"></p>
						<span class="content__item__time"></span>

					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<!--SPEAKERS START -->
	<h2 id="speakers">спикеры конференции</h2>
	<section class="speakers container clearfix">
		<div class="clearfix">
			<div class="speakers__details speakers__details__one">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/chudnyavcev.png" alt="сергей чуднявцев">
				</figure> 
				<h3>Сергей Чуднявцев</h3>
				<p><p>Психолог, семейный терапевт. Кандидат медицинских наук. Автор модели структуры конфликта и взаимодействия его компонентов. Автор модели «Парабола возраста», объясняющей закономерности развития родительско-детских отношений. В настоящее время является тренером и консультантом в компании Pucelik Consulting Group.
				</p>
			</div>
			<div class="speakers__details speakers__details__two">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/sudarkin.png" alt="александр сударкин">
				</figure> 
				<h3>Александр Сударкин</h3>
				<p>Бизнес-тренер, консультант по переговорам и организационному менеджменту компании Pucelik Consulting Group, автор публикаций в научных и бизнес-изданиях (в том числе HRD), специалист в области переговоров и организационного развития, успешно работал с крупными и средними предприятиями стран СНГ, постоянный спикер на различных конференциях (менеджмент, продажи, переговоры, организационное развитие)

				</p>
			</div>
		</div>
		<div class="clearfix">
			<div class="speakers__details speakers__details__three">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/brovchenko.png" alt="наталия бровченко">
				</figure> 
				<h3>Наталия Сирица (Бровченко)</h3>
				<p>Коуч международной категории (ECF), бизнес-тренер, HR-консультант, автор и ведущая программ по гармонизации жизни, карьерному коучингу, раскрытию потенциала. Более 10 лет опыта в сфере HR (Банк «Эталон», ЗАТ ФК «Динамо Киев», Wella AG, с 2007 года – HR-директор в rabota.ua.) Эксперт и спикер на международных и украинских конференциях, в СМИ и на ТВ (Интер, Украина, 1+1, UBR, 5 канал, Новый канал и другие).
				</p>
			</div>
			<div class="speakers__details speakers__details__four">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/mironov.png" alt=" михаил миронов">
				</figure> 
				<h3>Михаил Миронов</h3>
				<p>
					Cооснователь и генеральный директор Brainify.ru, выпускник организации AIESEC в России,  занимается проблемами образования с 2012 г.
				</p>
			</div>
		</div>
		<div class="clearfix">
			<div class="speakers__details speakers__details__five">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/zakarcheva.png" alt="виктория закарчевная">
				</figure> 
				<h3>Виктория Закарчевная</h3>
				<p>
					Руководитель направления подбора персонала и консалтинга в компании Newbreed, психолог, бизнес-тренер. Опыт работы более 5-ти лет. Опыт оценки кандидатов от начального до топ-уровня. Более 1000 проведенных собеседований в год. Автор курса «Школа рекрутмента» для начинающих специалистов и профессионалов.
				</p>
			</div>
			<div class="speakers__details speakers__details__six">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/suromyatnikova.png" alt="яна сыромятникова">
				</figure> 
				<h3>Яна Сыромятникова </h3>
				<p>
					Cовладелица свадебного агентства «ZEFIR», член Международной свадебной федерации, совладелица и преподаватель «Свадебной Школы», автор 5 книг о свадьбах и более 80 статей.
				</p>
			</div>
		</div>
		<div class="clearfix">
			<div class="speakers__details speakers__details__five">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/dovgal.png" alt="денис довгаль">
				</figure> 
				<h3>Денис Довгаль</h3>
				<p>Предприниматель, владелец продакшн-компании по созданию рисованных видео для бизнеса DOODLEVIDEO.RU и компании MOBILEVIDEO.PRO. Создатель и идеолог международного сообщества предпринимателей, которые создают и развивают свой географически свободный бизнес «TravelMBA».
				</p>
			</div>


			<div class="speakers__details speakers__details__five">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/petrova.png" alt="анна петрова">
				</figure> 
				<h3>Анна Петрова</h3>
				<p>Основатель и генеральный директор центра предпринимательства «Startup Ukraine» и коворкинга «Kyivworking», автор и организатор бизнес-лагеря «Made in Ukraine» и конференции «Entrepreholic!» , основатель сообщества «Selfmade Woman» и фонда  «Proud Foundation», автор молодежного проекта «Startup Youth», совладелица рекламного агентства «Business Icon».
				</p>
			</div>
		</div>
		<div class="clearfix">
			<div class="speakers__details speakers__details__five">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/zub.png" alt="орест зуб">
				</figure> 
				<h3>Орест Зуб</h3>
				<p>Путешественник (посетил 66 стран), блогер и онлайн-предприниматель. Основатель двух онлайн-бизнесов: Open Mind (http://openmind.com.ua) и Ukraine Travel Secrets (http://ukraine-travel-secrets.com/). Зарабатывает дистанционно, путешествуя при этом по 100+ дней в году.
				</p>
			</div>

			<div class="speakers__details speakers__details__five">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/zara.png" alt="зарангиз хусейнова">
				</figure> 
				<h3>Зарангиз Апетрей Хусейнова</h3>
				<p>Основатель первого в Азербайджане женского сообщества «WoWoman», основатель и генеральный директор бутика вечерних и свадебных платьев «ALZA», совладелица агентства цифрового маркетинга «Pulse». Инициатор развития молодежного движения в Баку, выпускница организации AIESEC и стипендиат программы развития от Департамента США, спикер на многочисленных национальных и международных конференциях.</p>
			</div>
			<div class="speakers__details speakers__details__five">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/shlapay.png" alt="зарангиз хусейнова">
				</figure> 
				<h3>Любовь Шлапай</h3>
        <p>Маркетинг-директор Академии Mindvalley (Малайзия), автор книги «Путь к мужчине», фанат-исследователь личностного роста.
				</p>
			</div>
			<div class="speakers__details speakers__details__five">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/agoeva.png" alt="зарангиз хусейнова">
				</figure> 
				<h3>Яна Агоева</h3>
				<p>Эксперт по системному росту и мотивации личности, бизнес-тренер по управлению персоналом, директор компании «Direct»: ТОП-рекрутинг и организация event-мероприятий. Основатель и президент HR-club Odessa, автор и ведущая проекта «HR-встречи с Агоевой Яной» (г. Днепропетровск).
				</p>
			</div>
			<div class="speakers__details speakers__details__five">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/burdeinaya.png" alt="зарангиз хусейнова">
				</figure> 
				<h3>Алёна Бурдейная</h3>
        <p>Cооснователь клуба любителей бега «KM Running Club», идейный основатель первой школы Рекрутинга в Украине «FSR.in.UA», основатель проекта «Школа здоровья - Health-SELF-Management», пробежала 5 марафонов
				</p>
			</div>
			<div class="speakers__details speakers__details__five">
				<figure class="speakers__img">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/lobova.png" alt="лена лобова">
				</figure> 
				<h3>Лена Лобова</h3>
        <p>COO и управляющий партнер международной компании «iLogos», сооснователь и координатор игровых конференций «Get IT!» и «Get App!». На протяжении последних четырех лет управляет всей операционной деятельностью компании, курирует экспансию в новые города и популяризацию GameDev в массах. Неисправимый перфекционист, стратег и аналитик. 
				</p>
			</div>
		</div>
	</section> <!-- end speakers -->
	
	<!-- PARTNERS START -->
	<h2>партнеры</h2>
	<div class="partner">
		<div class="container clearfix">
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/zefir.jpg" alt="zefir">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/masters.jpg" alt="мастера своего дела">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/newbreed.jpg" alt="newbreed">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/ilogos.jpg" alt="ilogos">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/hub.jpg" alt="hub odessa">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/preobragarium.jpg" alt="преображариум">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/hr-club.jpg" alt="hr-club">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/jansen.jpg" alt="jansen">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/krok.jpg" alt="Ассоциация выпускников КРОК">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/PCG.jpg" alt="PCG">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/startup_ukraine.jpg" alt="startup ukraine">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/brainify.jpg" alt="btainify">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/aiesec.jpg" alt="aiesec">
			</figure>
			<figure class="partner__logo">
				<img src="<?php echo bloginfo('template_url'); ?>/assets/img/travel-mba.jpg" alt="travel mba">
			</figure>
		</div>
	</div>
	
	<!-- Registration START -->	
	<div id="registration" class="registration">
		<h2>регистрация</h2>
		<div class="container prices">
			<div class="prices__item prices__item--first">
				<p class="prices__item__title">light</p>
				<p class="prices__item__price">
					<span>
						0
					</span>
					(0 UAH)
				</p>
				<p class="prices__item__description">участие в одном блоке конференции</p>
				<p class="prices__item__description prices__item__description--border-btn">без доступа к материалам</p>
				<button class="prices__item__button"><a style="color: #fff;" href="https://docs.google.com/forms/d/1J-ml6UUO7pT-tt0RsMpR1maz2zigZjPKL1ODpf-e8cg/viewform">КУПИТЬ</a></button>
			</div>
			<div class="prices__item prices__item--second">
				<p class="prices__item__title">early bird</p>
				<p class="prices__item__price">
					<span>
						3
					</span>
					(60 UAH)
				</p>
				<p class="prices__item__description">участие во всех блоках конференции</p>
				<p class="prices__item__description prices__item__description--border-btn">доступ ко всем материалам</p>
				<button class="prices__item__button"><a style="color: #fff;" href="https://docs.google.com/forms/d/1SxKIMCHvl1yEyP-umwg_MNZojlvByHG6QlHHBHbXzKE/viewform">КУПИТЬ</a></button>
			</div>
			<div class="prices__item prices__item--third">
				<p class="prices__item__title">basic</p>
				<p class="prices__item__price">
					<span>
						5
					</span>
					(100 UAH)
				</p>
				<p class="prices__item__description">участие во всех блоках конференции</p>
				<p class="prices__item__description prices__item__description--border-btn">доступ ко всем материалам</p>
				
<!--				<button class="prices__item__button">КУПИТЬ</button>-->
			</div>
			
		</div>
	</div>
	
	
	
	<!-- REVIEWS START -->
	
	<section class="reviews clearfix">
		<div class="container">
			<h2 class="reviews__title">Участники о конференции PROFORUM 2015</h2>
			<div class="review review__one">
				<p>
					«Очень довольна посещением ProForum 2015! Интересные спикеры, содержательные выступления, практические советы и истории успеха очень вдохновили меня к действиям. Я убедилась, что двигаюсь в правильном направлении. Конференция помогла мне найти ответы на свои вопросы. Я узнала много интересных для меня людей. Спасибо организаторам за чудесное мероприятие! Успехов и дальнейшего развития Вам!»
				</p>
				<p class="review__author">
				Ирина Белая
				</p>
			</div>
			<div class="review review__two">
				<p>
					«Мероприятие подарило мне много полезных мыслей и новых идей, массу ценных знакомств, помогло обратить внимание на мои профессиональные интересы и возможности. ProForum - это интересные, вдохновляющие и глубокие выступления, качественная и продуманная организация, возможность пообщаться с представителями бизнеса и оценить себя как потенциального работника. И, конечно, ещё долго не забуду десятки воздушных шаров с мечтами участников, которые были отпущены в небо в конце мероприятия. Настоятельно рекомендую ProForum для каждого, кто находится в поиске своего профессионального призвания».
				</p>
				<p class="review__author">
				Константин Шевчук
				</p>
			</div>
			<div class="review review__three">
				<p>
					«Я уже более 3х лет работаю в сфере и на позиции, о которых мечтала с начальных курсов университета. Казалось бы - что ещё нужно? Но мысли о том, что мои мечты стали результатом сочетания внешних обстоятельств и влияния авторитетных для меня людей, не давали покоя. В поисках себя, я начала посещать курсы и тренинги, узнавать о разных профессиях и заниматься творчеством. Как нельзя вовремя узнала о конференции "ProForum" и решила воспользоваться этой возможностью. Мои ожидания не были высокими, ведь я даже не думала, что там можно узнать что-то новое. Спикеры-психологи в своих выступлениях задавали правильные вопросы, в их историях я узнавала себя и со стороны могла идентифицировать базовые ошибки при выборе вуза. Я окунулась в детство и вспомнила себя настоящую. Прекрасная атсмосфера конференции позволила настроиться на нужный лад и познакомиться с собой заново! Теперь я знаю ответы на мучавшие меня вопросы! Спасибо организаторам за чудесную возможность высвободить своего внутреннего ребёнка и преобразовать жизнь из крысиных бегов в увлекательное путешествие!»
				</p>
				<p class="review__author">
				Ольга Дитмарова
				</p>
			</div>
	
		</div>
	</section> <!-- reviews end -->
	
	
	<!-- CONTACTS START -->
	
	<div class="contacts">
		<h2>контакты</h2>
		<div class="contacts__item">
			<p><span class="contacts__item__name">АННА СОЛОИД, организатор</span><br>proforum@start2go.com.ua<br>+38 063 7 355 433</p>
		</div>
	</div> <!-- contacts end -->
  
<?php get_footer(); ?>