<!doctype html>
<html class="no-js" lang="ru">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>
    <link rel="shortcut icon" href="<?php echo bloginfo('template_url'); ?>/assets/img/logo1.png"> 
		<meta property="og:site_name" content="Online Proforum" />
		<meta property="og:title" content="Международная online-конференция о призвании и выборе профессии" />  
		<meta property="og:description" content="«Ведь жизнь слишком коротка, чтобы делать то, что тебе не нравится!»" />  
		<meta property="og:type" content="website" />  
		<meta property="og:image" content="<?php echo bloginfo('template_url'); ?>/assets/img/logo_fb.jpg" />  
    <?php wp_head(); ?>
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63577194-1', 'auto');
  ga('send', 'pageview');

		</script>
  </head>

 <body>
  <header class="clearfix">
    <div class="header clearfix">
      <div class="header__logo"><a href="/"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/logo.jpg" alt="Proforum online" title="Proforum online"></a></div>
      <div class="header__collapse-btn"><p>☰</p></div>
      <nav class = "header__nav clearfix">
        <ul>
<!--          <li class="nav__link"><a href="/">Главная</a></li>-->
          <li class="nav__link"><a href="#about">Что это?</a></li>
          <li class="nav__link"><a href="#program">Программа</a></li>
          <li class="nav__link"><a href="#speakers">Спикеры</a></li>
          <li class="nav__link"><a href="#registration">Стоимость</a></li>
          <li class="nav__link"><a href="#rewards">Результаты</a></li>
          <li class="nav__link"><a href="#registration">Регистрация</a></li>
					<li class="nav__link nav__link--btn"><a style="color: #fff;" href="https://docs.google.com/forms/d/1SxKIMCHvl1yEyP-umwg_MNZojlvByHG6QlHHBHbXzKE/viewform">КУПИТЬ БИЛЕТ</a></li>
        </ul>
      </nav>
    </div>
  </header> <!-- HEADER END -->

