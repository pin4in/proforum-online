<div class="footer">
		<div class="container clearfix">
			<figure class="footer__logo">
				<a href="https://www.facebook.com/START2G0" target="_blank">
					<img src="<?php echo bloginfo('template_url'); ?>/assets/img/logo-footer.png" alt="start2go">
				</a>
			</figure>
			<div class="footer__contacts">
				<p>
					<span class="footer__contacts__phone">
						+38 063 7 355 433
					</span>
					<br>
					<span class="footer__contacts__email">
						proforum@start2go.com.ua
					</span>
				</p>
			</div>
			<div class="footer__social-nav clearfix">
				<ul class="social-nav clearfix">
					<li class="social-nav__link">
						<a href="https://www.facebook.com/START2G0" target="_blank">
							<img class="social-nav__link__icon" src="<?php echo bloginfo('template_url'); ?>/assets/img/facebook.png" alt="">
						</a>
					</li>
					<li class="social-nav__link">
						<a href="https://www.youtube.com/channel/UCgXqJST7arHdEf-phy1q_xw" target="_blank">
							<img class="social-nav__link__icon" src="<?php echo bloginfo('template_url'); ?>/assets/img/youtube.png" alt="">
						</a>
					</li>
					<li class="social-nav__link">
						<a href="http://vk.com/start_2_go" target="_blank">
							<img class="social-nav__link__icon" src="<?php echo bloginfo('template_url'); ?>/assets/img/vk.png" alt="">
						</a>
					</li>
				</ul>
			</div>
		</div>
		<p class="footer__copyright">All contents © copyright 2015, Start2Go</p>
	</div>
	<div class="back-to-top"></div>	
	<?php wp_footer(); ?>
	
</body>
</html>