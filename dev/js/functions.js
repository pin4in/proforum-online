jQuery( document ).ready(function() {

	console.log('scripts are loaded!');
	
	// menu collapse button
	var collaseButton = '.header__collapse-btn';
	var collapseActive = 'header__collapse-btn--active';
	
	jQuery(collaseButton).click(function(e) {
		
		e.preventDefault();
		console.log('click')
		
		if(jQuery(collaseButton).hasClass(collapseActive) == true) {
			
			console.log('hasClass');
			jQuery(collaseButton).removeClass(collapseActive);
			jQuery('.header__nav').slideToggle( "fast", "linear");
		}
		else{
			console.log('noclass')
			jQuery(collaseButton).addClass(collapseActive);
			jQuery('.header__nav').slideToggle( "fast", "linear" );
		}
		
		
		
	});
	
	
	// programm tabs
	jQuery('.nav__tab a').click(function (e) {
		
		e.preventDefault();
		jQuery('.nav__tab').removeClass('nav__tab__active');
		jQuery(this).closest('.nav__tab').addClass('nav__tab__active');
		
		jQuery('.program__content__tab').removeClass('program__content__tab__active')
		
		var target = $(this).first().attr('aria-controls');
		console.log(target);
		jQuery('#'+target).addClass('program__content__tab__active')	
		
	});
//		sticky header
	$(window).scroll(function() {  
		var x = $(this).scrollTop();
		if(x>50) {
			$("header").addClass("sticky");
		}
		else {
     $("header").removeClass("sticky");
		}		
	});

// to the top	
	

	var offset = 350;
	var duration = 800;
	var phoneW = 568;
	if($(window).width() > phoneW){
		$(window).scroll(function() {
				if ($(this).scrollTop() > offset) {
						$('.back-to-top').fadeIn(duration);
				} else {
						$('.back-to-top').fadeOut(duration);
				}
		});
	}

	$('.back-to-top').click(function(event) {
			event.preventDefault();
			$('html, body').animate({scrollTop: 0}, duration);
			return false;
	})
	
	

	
	
	
	
});